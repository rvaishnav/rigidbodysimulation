/*

  USC/Viterbi/Computer Science
  "Jello Cube" Assignment 1 starter code

*/

#include "jello.h"
#include "physics.h"

double  left=-2.0;
double	bottom=-2.0;
double	back=-2.0;
double  right=2.0;
double	top=2.0;
double	front=2.0;
double restlength= 1.0/7.0;

/* Computes acceleration to every control point of the jello cube, 
   which is in state given by 'jello'.
   Returns result in array 'a'. */
void computeAcceleration(struct world * jello, struct point a[8][8][8])
{
  /* for you to implement ... */
	// For each mass point
		point force[8][8][8];
	for (int i=0; i<=7; i++)
     for (int j=0; j<=7; j++)
      for (int k=0; k<=7; k++)
      {
		  a[i][j][k].x = 0.0;
		  a[i][j][k].y = 0.0;
		  a[i][j][k].z = 0.0;
	  }
	
	  // calculate value for incline plane
	 
	  double planevalue= jello->a * jello->p[0][0][0].x + jello->b * jello->p[0][0][0].y + jello->c * jello->p[0][0][0].z + jello->d;
	  double ptplval;
	  point planenormal;
	  planenormal.x=jello->a;
	  planenormal.y = jello->b;
	  planenormal.z = jello->c;
	  // normal value can be used to calculate distance to plane
	  double normalvalue=jello->a * planenormal.x + jello->b * planenormal.y + jello->c * planenormal.z + jello->d;
	  if (( normalvalue >0.0 && planevalue <0.0)||(normalvalue <0.0 && planevalue >0.0))
	  {
		  pMULTIPLY(planenormal,-1.0,planenormal);
	  }

	

	//initial force to zero

for (int i=0; i<=7; i++)
     for (int j=0; j<=7; j++)
      for (int k=0; k<=7; k++)
      {
		  force[i][j][k].x = 0.0;
		  force[i][j][k].y = 0.0;
		  force[i][j][k].z = 0.0;
		  //variables for collision
		  point collsionf={0};
		  point outpos, outvel;
		  outpos = jello->p[i][j][k];
		  outvel = jello->v[i][j][k];


	//starting with structural springs 
	// In general there are 6 structural springs for a mass point unless it is in one of the faces 
	// one less spring for each face it has common
	
		  {
			  if(i>0)
			  {
				  pSUM(force[i][j][k], hookndamp(i,j,k,i-1,j,k, restlength), force[i][j][k]);
			  }
			  if(i<7)
			  {
				  pSUM(force[i][j][k], hookndamp(i,j,k,i+1,j,k, restlength), force[i][j][k]);
			  }
			  if(j>0)
			  {
				  pSUM(force[i][j][k], hookndamp(i,j,k,i,j-1,k, restlength), force[i][j][k]);
			  }
			  if(j<7)
			  {
				  pSUM(force[i][j][k], hookndamp(i,j,k,i,j+1,k, restlength), force[i][j][k]);
			  }
			  if(k>0)
			  {
				  pSUM(force[i][j][k], hookndamp(i,j,k,i,j,k-1, restlength), force[i][j][k]);
			  }
			  if(k<7)
			  {
				  pSUM(force[i][j][k], hookndamp(i,j,k,i,j,k+1, restlength), force[i][j][k]);
			  }
		  }

	

	//Shear Springs
	// There are 2 types of shear springs , one along face diagonal and one along body diagonal
	// the length of the one along the face is sqrt 2 times side lenghth 
	//and of body diagonal is sqrt 3 times side length
	// First dealing with face diagonals
	// There are 12 of them, 4 in each plane

		  {
			  if(i>0)
			  {
				  if(j>0)
				  {
					  pSUM(force[i][j][k], hookndamp(i,j,k,i-1,j-1,k, sqrtl(2)*restlength), force[i][j][k]);

					  if(k>0)
					  {
						  pSUM(force[i][j][k], hookndamp(i,j,k,i-1,j-1,k-1, sqrtl(3)*restlength), force[i][j][k]);
					  }
					  if(k<7)
					  {
						  pSUM(force[i][j][k], hookndamp(i,j,k,i-1,j-1,k+1, sqrtl(3)*restlength), force[i][j][k]);
					  }
				  }
				  if(j<7)
				  {
					  pSUM(force[i][j][k], hookndamp(i,j,k,i-1,j+1,k, sqrtl(2)*restlength), force[i][j][k]);

					  if(k>0)
					  {
						  pSUM(force[i][j][k], hookndamp(i,j,k,i-1,j+1,k-1, sqrtl(3)*restlength), force[i][j][k]);
					  }
					  if(k<7)
					  {
						  pSUM(force[i][j][k], hookndamp(i,j,k,i-1,j+1,k+1, sqrtl(3)*restlength), force[i][j][k]);
					  }
				  }

				  if(k>0)
				  {
					  pSUM(force[i][j][k], hookndamp(i,j,k,i-1,j,k-1, sqrtl(2)*restlength), force[i][j][k]);
				  }
				  if(k<7)
				  {
					  pSUM(force[i][j][k], hookndamp(i,j,k,i-1,j,k+1, sqrtl(2)*restlength), force[i][j][k]);
				  }
			  }
			  if(i<7)
			  {
				  if(j>0)
				  {
					  pSUM(force[i][j][k], hookndamp(i,j,k,i+1,j-1,k, sqrtl(2)*restlength), force[i][j][k]);

					  if(k>0)
					  {
						  pSUM(force[i][j][k], hookndamp(i,j,k,i+1,j-1,k-1, sqrtl(3)*restlength), force[i][j][k]);
					  }
					  if(k<7)
					  {
						  pSUM(force[i][j][k], hookndamp(i,j,k,i+1,j-1,k+1, sqrtl(3)*restlength), force[i][j][k]);
					  }
				  }
				  if(j<7)
				  {
					  pSUM(force[i][j][k], hookndamp(i,j,k,i+1,j+1,k, sqrtl(2)*restlength), force[i][j][k]);
				  				  
					  if(k>0)
					  {
						  pSUM(force[i][j][k], hookndamp(i,j,k,i+1,j+1,k-1, sqrtl(3)*restlength), force[i][j][k]);
					  }
					  if(k<7)
					  {
						  pSUM(force[i][j][k], hookndamp(i,j,k,i+1,j+1,k+1, sqrtl(3)*restlength), force[i][j][k]);
					  }
				  }
				  if(k>0)
				  {
					  pSUM(force[i][j][k], hookndamp(i,j,k,i+1,j,k-1, sqrtl(2)*restlength), force[i][j][k]);
				  }
				  if(k<7)
				  {
					  pSUM(force[i][j][k], hookndamp(i,j,k,i+1,j,k+1, sqrtl(2)*restlength), force[i][j][k]);
				  }
			  }
			  if(j>0)
			  {
				  if(k>0)
				  {
					  pSUM(force[i][j][k], hookndamp(i,j,k,i,j-1,k-1, sqrtl(2)*restlength), force[i][j][k]);
				  }
				  if(k<7)
				  {
					  pSUM(force[i][j][k], hookndamp(i,j,k,i,j-1,k+1, sqrtl(2)*restlength), force[i][j][k]);
				  }
			  }
			  if(j<7)
			  {
				  if(k>0)
				  {
					  pSUM(force[i][j][k], hookndamp(i,j,k,i,j+1,k-1, sqrtl(2)*restlength), force[i][j][k]);
				  }
				  if(k<7)
				  {
					  pSUM(force[i][j][k], hookndamp(i,j,k,i,j+1,k+1, sqrtl(2)*restlength), force[i][j][k]);
				  }
			  }
		  }
		  

	//these are bend springs 
	// connects a point to its 2nd neighbour, 6 in general
		  {
			  if(i>1)
			  {
				  pSUM(force[i][j][k], hookndamp(i,j,k,i-2,j,k, 2*restlength), force[i][j][k]);
			  }
			  if(i<6)
			  {
				  pSUM(force[i][j][k], hookndamp(i,j,k,i+2,j,k, 2*restlength), force[i][j][k]);
			  }
			  if(j>1)
			  {
				  pSUM(force[i][j][k], hookndamp(i,j,k,i,j-2,k, 2*restlength), force[i][j][k]);
			  }
			  if(j<6)
			  {
				  pSUM(force[i][j][k], hookndamp(i,j,k,i,j+2,k, 2*restlength), force[i][j][k]);
			  }
			  if(k>1)
			  {
				  pSUM(force[i][j][k], hookndamp(i,j,k,i,j,k-2, 2*restlength), force[i][j][k]);
			  }
			  if(k<6)
			  {
				  pSUM(force[i][j][k], hookndamp(i,j,k,i,j,k+2, 2*restlength), force[i][j][k]);
			  }
		  }

		  

		  
		  //collision detection and response

		  {			  
			  // deal with each component seperately and compute the respective forces
			  // resultant vector will be the collision force
			
				if(outpos.x<=-2)
				{
					collsionf.x = - jello->kCollision * (outpos.x+2);
					collsionf.x += -jello->dCollision * outvel.x;
					pSUM(force[i][j][k], collsionf, force[i][j][k]);
				}
			
				if(outpos.x>=2)
				{
					collsionf.x = - jello->kCollision * (outpos.x-2);
					collsionf.x += -jello->dCollision * outvel.x;
					pSUM(force[i][j][k], collsionf, force[i][j][k]);
				}
			
				if(outpos.y<=-2)
				{
					collsionf.y = - jello->kCollision * (outpos.y+2);
					collsionf.y += -jello->dCollision * outvel.y;
					pSUM(force[i][j][k], collsionf, force[i][j][k]);
				}
				if(outpos.y>=2)
				{
					collsionf.y = - jello->kCollision * (outpos.y-2);
					collsionf.y += -jello->dCollision * outvel.y;
					pSUM(force[i][j][k], collsionf, force[i][j][k]);
				}
			
				if(outpos.z<=-2)
				{
					collsionf.z = - jello->kCollision * (outpos.z+2);
					collsionf.z += -jello->dCollision * outvel.z;
					pSUM(force[i][j][k], collsionf, force[i][j][k]);
				}
				if(outpos.z>=2)
				{
					collsionf.z = - jello->kCollision * (outpos.z-2);
					collsionf.z += -jello->dCollision * outvel.z;
					pSUM(force[i][j][k], collsionf, force[i][j][k]);
				}
		  }
		
		  // external force field
	
				int x1, x2, y1, y2, z1, z2;
				double  alpha, beta, gamma;
				double jres= jello->resolution-1;
				double xscaled, yscaled, zscaled;
				point pos = jello->p[i][j][k];
				double boxdim=4.0;
				double scalar= jello->resolution/boxdim; // here 4 is the dimension of the bounding box
				// we need to scale the point to proper resolution then use trilinear interpolation
				xscaled = (pos.x + 2) * scalar;
				yscaled = (pos.y + 2) * scalar;
				zscaled = (pos.z + 2) * scalar;

				x1 = floor(xscaled);
				x2 = ceil(xscaled);
				y1 = floor(yscaled);
				y2 = ceil(yscaled);
				z1 = floor(zscaled);
				z2 = ceil(zscaled);

				alpha = xscaled - x1;
				beta = yscaled - y1;
				gamma = zscaled - z1;

			
				if(x1<jres && y1<jres && z1<jres && x1>=0 && y1>=0 && z1>=0)
				{
					point fcomp000,fcomp001,fcomp010,fcomp011,fcomp100,fcomp101,fcomp110,fcomp111;
					fcomp000=jello->forceField[x1 * jello->resolution * jello->resolution + y1 * jello->resolution + z1];
					fcomp001=jello->forceField[x1 * jello->resolution * jello->resolution + y1 * jello->resolution + z2];
					fcomp010=jello->forceField[x1 * jello->resolution * jello->resolution + y2 * jello->resolution + z1];
					fcomp011=jello->forceField[x1 * jello->resolution * jello->resolution + y2 * jello->resolution + z2];
					fcomp100=jello->forceField[x2 * jello->resolution * jello->resolution + y1 * jello->resolution + z1];
					fcomp101=jello->forceField[x2 * jello->resolution * jello->resolution + y1 * jello->resolution + z2];
					fcomp110=jello->forceField[x2 * jello->resolution * jello->resolution + y2 * jello->resolution + z1];
					fcomp111=jello->forceField[x2 * jello->resolution * jello->resolution + y2 * jello->resolution + z2];
					
					point netextforce={0};
					pSUM(netextforce,fcomp000,netextforce);
					pSUM(netextforce,fcomp001,netextforce);
					pSUM(netextforce,fcomp010,netextforce);
					pSUM(netextforce,fcomp011,netextforce);
					pSUM(netextforce,fcomp100,netextforce);
					pSUM(netextforce,fcomp101,netextforce);
					pSUM(netextforce,fcomp110,netextforce);
					pSUM(netextforce,fcomp111,netextforce);
					
					pSUM(force[i][j][k], netextforce, force[i][j][k]);
				}		    
		  
				// Inclined plane 
				 // check for collision with inclined plane 
				ptplval= jello->a * jello->p[i][j][k].x + jello->b * jello->p[i][j][k].y + jello->c * jello->p[i][j][k].z + jello->d;
		
			/*	if((ptplval>0.0 && planevalue<0.0)||(ptplval<0.0 && planevalue>0.0))//collsion occured
				{
					float dist= ptplval/sqrt(normalvalue);
					if(dist<0.0)
						dist=dist*-1;
					point nownormal;
					pCPY(planenormal,nownormal);
					// normalised normal vector and then scaled to point
					pMULTIPLY(nownormal,-dist/sqrt(normalvalue),nownormal);
					point footpoint;
					pDIFFERENCE(jello->p[i][j][k], nownormal, footpoint);// this is the foot of the perpendicular that lies on inclined plane 
					point btoa;
					pDIFFERENCE(jello->p[i][j][k], footpoint, btoa); //Vector B->A
					pMULTIPLY(btoa, -jello->kElastic*0.1, btoa);
					double ldotn= btoa.x*planenormal.x + btoa.y*planenormal.y + btoa.z*planenormal.z;
						   	
					pMULTIPLY(planenormal, ldotn, btoa); 
					pSUM(force[i][j][k], btoa, force[i][j][k]);

				}
				
				*/
				// extracredit point mouse dragging
				if(mousedrag)
					{
						// dragging with mouse will cause acceleration in negative x and y direction
						force[i][j][k].x -= deltax*4;
						force[i][j][k].z -= deltay*4;
					}


		  // finally calculate acceleration from the mass of each control point 
				pMULTIPLY(force[i][j][k],1/jello->mass,a[i][j][k]);
   }
   mousedrag=0;
}
//This function computes 3d hook force and damp force from the positions of particles and rest length of spring
// It then uses mass point to compute acceleration from the force using newton 2nd law


/* performs one step of Euler Integration */
/* as a result, updates the jello structure */
void Euler(struct world * jello)
{
  int i,j,k;
  point a[8][8][8]={0};

  computeAcceleration(jello, a);
 

  for (i=0; i<=7; i++)
    for (j=0; j<=7; j++)
      for (k=0; k<=7; k++)
      {
        jello->p[i][j][k].x += jello->dt * jello->v[i][j][k].x;
        jello->p[i][j][k].y += jello->dt * jello->v[i][j][k].y;
        jello->p[i][j][k].z += jello->dt * jello->v[i][j][k].z;
        jello->v[i][j][k].x += jello->dt * a[i][j][k].x;
        jello->v[i][j][k].y += jello->dt * a[i][j][k].y;
        jello->v[i][j][k].z += jello->dt * a[i][j][k].z;

      }
	
}

/* performs one step of RK4 Integration */
/* as a result, updates the jello structure */
void RK4(struct world * jello)
{
  point F1p[8][8][8], F1v[8][8][8], 
        F2p[8][8][8], F2v[8][8][8],
        F3p[8][8][8], F3v[8][8][8],
        F4p[8][8][8], F4v[8][8][8];

  point a[8][8][8];


  struct world buffer;

  int i,j,k;

  buffer = *jello; // make a copy of jello

  computeAcceleration(jello, a);

  for (i=0; i<=7; i++)
    for (j=0; j<=7; j++)
      for (k=0; k<=7; k++)
      {
         pMULTIPLY(jello->v[i][j][k],jello->dt,F1p[i][j][k]);
         pMULTIPLY(a[i][j][k],jello->dt,F1v[i][j][k]);
         pMULTIPLY(F1p[i][j][k],0.5,buffer.p[i][j][k]);
         pMULTIPLY(F1v[i][j][k],0.5,buffer.v[i][j][k]);
         pSUM(jello->p[i][j][k],buffer.p[i][j][k],buffer.p[i][j][k]);
         pSUM(jello->v[i][j][k],buffer.v[i][j][k],buffer.v[i][j][k]);
      }

  computeAcceleration(&buffer, a);

  for (i=0; i<=7; i++)
    for (j=0; j<=7; j++)
      for (k=0; k<=7; k++)
      {
         // F2p = dt * buffer.v;
         pMULTIPLY(buffer.v[i][j][k],jello->dt,F2p[i][j][k]);
         // F2v = dt * a(buffer.p,buffer.v);     
         pMULTIPLY(a[i][j][k],jello->dt,F2v[i][j][k]);
         pMULTIPLY(F2p[i][j][k],0.5,buffer.p[i][j][k]);
         pMULTIPLY(F2v[i][j][k],0.5,buffer.v[i][j][k]);
         pSUM(jello->p[i][j][k],buffer.p[i][j][k],buffer.p[i][j][k]);
         pSUM(jello->v[i][j][k],buffer.v[i][j][k],buffer.v[i][j][k]);
      }

  computeAcceleration(&buffer, a);

  for (i=0; i<=7; i++)
    for (j=0; j<=7; j++)
      for (k=0; k<=7; k++)
      {
         // F3p = dt * buffer.v;
         pMULTIPLY(buffer.v[i][j][k],jello->dt,F3p[i][j][k]);
         // F3v = dt * a(buffer.p,buffer.v);     
         pMULTIPLY(a[i][j][k],jello->dt,F3v[i][j][k]);
         pMULTIPLY(F3p[i][j][k],0.5,buffer.p[i][j][k]);
         pMULTIPLY(F3v[i][j][k],0.5,buffer.v[i][j][k]);
         pSUM(jello->p[i][j][k],buffer.p[i][j][k],buffer.p[i][j][k]);
         pSUM(jello->v[i][j][k],buffer.v[i][j][k],buffer.v[i][j][k]);
      }
         
  computeAcceleration(&buffer, a);


  for (i=0; i<=7; i++)
    for (j=0; j<=7; j++)
      for (k=0; k<=7; k++)
      {
         // F3p = dt * buffer.v;
         pMULTIPLY(buffer.v[i][j][k],jello->dt,F4p[i][j][k]);
         // F3v = dt * a(buffer.p,buffer.v);     
         pMULTIPLY(a[i][j][k],jello->dt,F4v[i][j][k]);

         pMULTIPLY(F2p[i][j][k],2,buffer.p[i][j][k]);
         pMULTIPLY(F3p[i][j][k],2,buffer.v[i][j][k]);
         pSUM(buffer.p[i][j][k],buffer.v[i][j][k],buffer.p[i][j][k]);
         pSUM(buffer.p[i][j][k],F1p[i][j][k],buffer.p[i][j][k]);
         pSUM(buffer.p[i][j][k],F4p[i][j][k],buffer.p[i][j][k]);
         pMULTIPLY(buffer.p[i][j][k],1.0 / 6,buffer.p[i][j][k]);
         pSUM(buffer.p[i][j][k],jello->p[i][j][k],jello->p[i][j][k]);

         pMULTIPLY(F2v[i][j][k],2,buffer.p[i][j][k]);
         pMULTIPLY(F3v[i][j][k],2,buffer.v[i][j][k]);
         pSUM(buffer.p[i][j][k],buffer.v[i][j][k],buffer.p[i][j][k]);
         pSUM(buffer.p[i][j][k],F1v[i][j][k],buffer.p[i][j][k]);
         pSUM(buffer.p[i][j][k],F4v[i][j][k],buffer.p[i][j][k]);
         pMULTIPLY(buffer.p[i][j][k],1.0 / 6,buffer.p[i][j][k]);
         pSUM(buffer.p[i][j][k],jello->v[i][j][k],jello->v[i][j][k]);
      }

  return;  
}


// This method is used to calculate hook force and damp force , it reads the identifiers for 2 points and the rest length for springs so that we can call it for any type of spring
// by specifying the rest length of spring
// It returns the sum of hook and damping force as a vector
point hookndamp(int ai, int aj, int ak, int bi, int bj, int bk, double restlength)
{
	point hookforce, dampingforce;// for hook force from and for damping force
	point  pointa, pointb; // the 2 points used to calculate hook and damping force
	point  vela, velb;
	point vdiff;// for difference between velocities of a and b
	point fresultant;
	double projV_L;

	// point a and b's positions and velocity for spring calculations
	pointa = jello.p[ai][aj][ak];
	pointb = jello.p[bi][bj][bk];
	

	// the actual calculations as explained in course slides 
	point L; 
	// the vector from point b to point a
	pDIFFERENCE(pointa, pointb, L);	
	//Magnitude of the vector
	double magneto = sqrt((L).x * (L).x + (L).y * (L).y + (L).z * (L).z);// squareroot of dot product of vector with itself
	
	double scalar = -jello.kElastic * ( magneto - restlength );    //  Force= -k * (|L|-R)
	
	//Force= Force * L/|L|
	pMULTIPLY(L,scalar/magneto,hookforce);
	
	
	//velocities of point a and b
	vela = jello.v[ai][aj][ak];
	velb = jello.v[bi][bj][bk];
	
	pDIFFERENCE(vela, velb, vdiff);// velocity difference i.e relative velocity
	projV_L = vdiff.x*L.x + vdiff.y*L.y + vdiff.z*L.z;// component of v on L
	scalar = -jello.dElastic * projV_L/ magneto;
	pMULTIPLY(L,scalar/magneto,dampingforce);

	// Finally sum the hook force from spring and the damping force due to velocity
	//to get resultant force
	pSUM(dampingforce,hookforce,fresultant);
	
	//return the resultant force	
	return fresultant;
}