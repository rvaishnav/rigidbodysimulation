<Please submit this file with your solution.>

CSCI 520, Assignment 1

<Your name>

================

<Description of what you have accomplished>
<Also, explain any extra credit that you have implemented.>



Name : Ravi Vaishnav 
USC id 8416272945

I followed a very simple approach in completing assignment.
While calculating forces i found it was easy for me to compute forces individually for each component (i.e x,y,z) and then sum them up. In this
way i avoided dealing with confusing vector arithmetic majority of time.
Everything was implemented as directed in class.

For Extra Credit:
For extra credit i included mouse dragging using glut motion function, i read displacement from last mouse position on screen to produce an acceleration. The acceleration are small in magnitude to avoid jerking or collapse.
Currently i have supported acceleration in +ve and -ve x and z axis...because camera is positioned with z point up so it made sense that displacement in Y to be used in provided acceleration vertical axis, an horizontal is same as on screen i.e x-axis
I have colored the 3 axes by standard color notation so that it is clearly visible which axis is which.

I have also included the inclined plane in the picture , it is not confined within the box , it is just an inclined plane which happens to pass through the bounding box. It seems there is a problem in it so sadly i have commented it out.
Thank you


